/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistaWeb;

import controlador.ControladorJuego;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import modelo.Carta;
import modelo.Participante;
import vista.IVistaJuego;

/**
 *
 * @author Polachek
 */
public class vJuego implements IVistaJuego{
    private ControladorJuego controlador;
    
    private PrintWriter out;
    private HttpServletRequest request;
    
    public void inicializar() {
        Participante miP = (Participante)request.getSession(false).getAttribute("participante");
        controlador = new ControladorJuego(miP, this);
    }

    @Override
    public void mostrarSaldo(int saldoActual) {
        enviar("mostrarSaldo",saldoActual+"");
    }

    @Override
    public void actualizarCartas(Carta[] cartas) {
        for (int i = 0; i < cartas.length; i++) {
            /* APLICAR 1 SOLO EVENTO 
             String strCarta1 = "" + ((cartas[i].getNumero()) + 2) + cartas[i].getPaloString().substring(0, 1);
                    strCarta1 = "assets/images/cartas" + strCarta1 + ".gif";
                    enviar("carta",strCarta1);*/
            switch (i) {
                case 0:
                    String strCarta1 = "" + ((cartas[i].getNumero()) + 2) + cartas[i].getPaloString().substring(0, 1);
                    strCarta1 = "assets/images/cartas" + strCarta1 + ".gif";
                    enviar("carta1",strCarta1);
                case 1:
                    String strCarta2 = "" + ((cartas[i].getNumero()) + 2) + cartas[i].getPaloString().substring(0, 1);
                    strCarta2 = "assets/images/cartas" + strCarta2 + ".gif";
                    enviar("carta2",strCarta2);
                case 2:
                    String strCarta3 = "" + ((cartas[i].getNumero()) + 2) + cartas[i].getPaloString().substring(0, 1);
                    strCarta3 = "assets/images/cartas" + strCarta3 + ".gif";
                    enviar("carta3",strCarta3);
                case 3:
                    String strCarta4 = "" + ((cartas[i].getNumero()) + 2) + cartas[i].getPaloString().substring(0, 1);
                    strCarta4 = "assets/images/cartas" + strCarta4 + ".gif";
                    enviar("carta4",strCarta4);
                case 4:
                    String strCarta5 = "" + ((cartas[i].getNumero()) + 2) + cartas[i].getPaloString().substring(0, 1);
                    strCarta5 = "assets/images/cartas" + strCarta5 + ".gif";
                    enviar("carta5",strCarta5);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void mostrarPozo(int pozoManoActual) {
        enviar("mostrarPozo",pozoManoActual+"");
    }

    @Override
    public void avisarQueHuboApuesta(int valor, String nombreCompletoDelJugador) {
        
    }

    @Override
    public void mostrarGanador(Participante p) {
        
    }

    @Override
    public void limpiar() {
        
    }

    @Override
    public void avisarQuePase() {
        
    }

    @Override
    public void mostrarLosQuePasaron(ArrayList<Participante> losQuepasan) {
        
    }

    @Override
    public void mostrarLosQueDejanLamano(ArrayList<Participante> losQueSeFueron) {
        
    }

    @Override
    public void seTerminoMiPartida(String mensaje, String titulo) {
        
    }

    @Override
    public void felicitarGanadorPartida(Participante p) {
        
    }

    @Override
    public void mostrarLomodeLasCartasYDesactivarBotones() {
        
    }

    @Override
    public void mostrarSaludo(String nombreCompletoDelJugador) {
        enviar("mostrarSaludo",nombreCompletoDelJugador.toUpperCase());
    }

    @Override
    public void mostrarMensajeError(String mensaje) {
        
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
             //System.out.println("Enviado");
        }
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {
        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        
    }
    
}
