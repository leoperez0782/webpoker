<%-- 
    Document   : juego
    Created on : 02-jul-2018, 16:55:57
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="styles/styles.css">
        <title>JSP Page</title>
    </head>
    
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript">
            var vistaWeb = new EventSource("juego");
            
            vistaWeb.onerror = function(evento) {
               alert("Sin conexion con el servidor o UnsupportedOperationException");
                vistaWeb.close();
                document.location="/";
            };
            
            //Mostrar Saludo
            vistaWeb.addEventListener("mostrarSaludo", function (evento){
                document.getElementById("usuario").innerHTML=evento.data;
                
            },false);
            
            //Mostrar Saldo
            vistaWeb.addEventListener("mostrarSaldo", function (evento){
                document.getElementById("saldoAct").innerHTML=evento.data;
                
            },false);
            
            //Mostrar Pozo
            vistaWeb.addEventListener("mostrarPozo", function (evento){
                document.getElementById("pozo").innerHTML=evento.data;
                
            },false);
            
            
            
            
            
            //CARTAS
            //Mostrar carta 1
            vistaWeb.addEventListener("carta1", function (evento){
                $("#carta1").attr("src", evento.data);      
                
            },false);
            
            //Mostrar carta 2
            vistaWeb.addEventListener("carta2", function (evento){
                $("#carta2").attr("src", evento.data);      
                
            },false);
            
            //Mostrar carta 3
            vistaWeb.addEventListener("carta3", function (evento){
                $("#carta3").attr("src", evento.data);      
                
            },false);
            
            //Mostrar carta 4
            vistaWeb.addEventListener("carta4", function (evento){
                $("#carta4").attr("src", evento.data);      
                
            },false);
            
            //Mostrar carta 5
            vistaWeb.addEventListener("carta5", function (evento){
                $("#carta5").attr("src", evento.data);                
            },false);
            
            
            
    </script>    
    
    <body class="juego">
        <h1>Bienvenido al juego</h1>
        <section id="area-juego">
            <div class="pozo">
                <h2>Pozo actual: $<span id="pozo"></span></h2>
            </div>
            <div class="cartas">
                <div><img id="carta1" src="" /></div>
                <div><img id="carta2" src="" /></div>
                <div><img id="carta3" src="" /></div>
                <div><img id="carta4" src="" /></div>
                <div><img id="carta5" src="" /></div>
            </div>
        </section>
        
        
        <section id="panelJugador">
            <div id="infoJug">
                <h3 id="usuario"></h3>
                <p>Saldo actual: $<span id="saldoAct"></span></p>
            </div>
            
            
            <div id="acciones"></div>
            <div id="mensajes"></div>
        </section>
    </body>
</html>
